package com.example;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        Map<String, String> result = new HashMap<>();

        int port = Integer.parseInt(System.getProperty("port", "8080"));
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(port), 0);
        HttpHandler httpHandler = exchange -> {
            try {
                int id = result.size() + 1;
                String key = "key" + id;
                System.out.println("processing " + exchange.getRequestURI());

                InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr);
                String body = br.readLine();

                Map<String, String> requestParams = Optional.ofNullable(exchange.getRequestURI().getQuery()).stream()
                        .flatMap(query -> Arrays.stream(query.split("&")))
                        .filter(param -> param.indexOf("=") >= 0)
                        .collect(Collectors.toMap(
                                param -> param.substring(0, param.indexOf("=")),
                                param -> param.substring(param.indexOf("=") + 1)
                        ));

                String resp = doBusiness(exchange.getRequestMethod(), body, requestParams, exchange.getRequestURI().toString(), result, key);

                String s = String.format("<html><body>%s</body></html>", resp);

                exchange.sendResponseHeaders(200, s.length());
                exchange.getResponseBody().write(s.getBytes());
                exchange.close();

                System.out.println(exchange.getRequestURI() + " processed");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        httpServer.createContext("/app", httpHandler);
        httpServer.start();

    }

    private static String doBusiness(String method, String body, Map<String, String> requestParams, String uri, Map<String, String> result, String key) {
        String resp = "Error. Method not used";
        switch (method) {
            case "GET": {
                if (uri.equals("/app/txts"))
                    resp = getText(result);
                else if (uri.startsWith("/app/txts/"))
                    resp = getTextByKey(result, requestParams);
                break;
            }
            case "POST": {
                if (uri.equals("/app/txts"))
                    resp = saveText(body, result, key);
                else if (uri.startsWith("/app/txts/"))
                    resp = updateText(body, result, requestParams);
                break;
            }
            case "DELETE": {
                if (uri.startsWith("/app/txts"))
                    resp = deleteText(result, requestParams);
                break;
            }
            default:
                resp = "Error. Method not found";
        }
        return resp;
    }

    private static String saveText(String body, Map<String, String> result, String id) {
        if (body == null || body.isEmpty()) return "Body is empty";
        Optional<String> chek = result.entrySet()
                .stream()
                .filter(entry -> body.equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst();

        if (chek.isPresent())
            return String.format("This text already exists. Key = %s", chek.get());
        else {
            result.put(id, body);
            return String.format("Key = %s", id);
        }
    }

    private static String getText(Map<String, String> result) {
        if (result.isEmpty())
            return "Map is empty";
        else {
            StringBuilder resultStr = new StringBuilder();
            for (Map.Entry<String, String> entry : result.entrySet()) {
                resultStr
                        .append(entry.getKey())
                        .append("=")
                        .append(entry.getValue())
                        .append("<br>");
            }
            return resultStr.toString();
        }
    }

    private static String getTextByKey(Map<String, String> result, Map<String, String> requestParams) {
        String key = requestParams.get("key");
        if (key == null) return "Input key is null";
        if (result.containsKey(key)) {
            String value = result.get(key);
            return String.format("Value = %s", value);
        } else return "Key is not found";
    }

    private static String deleteText(Map<String, String> result, Map<String, String> requestParams) {
        String key = requestParams.get("key");
        if (key == null) return "Input key is null";
        if (result.containsKey(key)) {
            result.remove(key);
            return String.format("Entry with key = %s deleted", key);
        } else return "Key is not found";
    }

    private static String updateText(String body, Map<String, String> result, Map<String, String> requestParams) {
        String key = requestParams.get("key");
        if (key == null) return "Input key is null";
        if (body == null || body.isEmpty()) return "Body is empty";

        Optional<String> chek = result.entrySet()
                .stream()
                .filter(entry -> body.equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst();

        if (chek.isPresent())
            return String.format("This text already exists. Key = %s", chek.get());
        else if (result.containsKey(key)) {
            result.put(key, body);
            return String.format("Entry with key = %s updated", key);
        } else return "Key is not found";
    }
}
